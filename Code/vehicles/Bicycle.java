/**
 *	Bicycle is a class that creates Bicycle object 
 *  @author   Kayci Davila
 *  @version  8/31/2022
*/
package vehicles;

public class Bicycle {

    private String manufacturer;
    private int numberGears;
    private double maxSpeed;

    //constructor
    public Bicycle (String manufacturer, int numberGears, double maxSpeed){
        this.manufacturer = manufacturer;
        this.numberGears = numberGears;
        this.maxSpeed = maxSpeed;
    }

    //getter methods
    public String getManufacturer (){
        return this.manufacturer;
    }

    public int getNumberGears(){
        return this.numberGears;
    }

    public double getMaxSpeed(){
        return this.maxSpeed;
    } 
    
    public String toString() {
        return "Manufacturer: "+ this.manufacturer + ", Number of Gears: " + this.numberGears + ", MaxSpeed: " + this.maxSpeed;
    }
}
