/**
 *	BikeStore is a class that tests my Bicycle class 
 *  @author   Kayci Davila
 *  @version  8/31/2022
*/
package application;
import vehicles.*;

public class BikeStore {
    public static void main (String [] args){
        Bicycle [] store = new Bicycle [4];
        store [0] = new Bicycle("Ayainc", 6 ,25.3 );
        store [1] = new Bicycle("ThomasInc",7,30.5 );
        store [2] = new Bicycle("ChrisInc",10, 15.8);
        store [3] = new Bicycle("LiliInc", 12,45 );

        //printing
        for (int x = 0; x < store.length;x++){
            System.out.println(store[x]);
        }
    }

}
